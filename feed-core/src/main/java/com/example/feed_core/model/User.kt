package com.example.feed_core.model

data class User(
    val id: Long,
    val displayName: String,
    val userName: String,
    val avatarImageUrl: String,
    val followers: Int,
    val following: Int,
    val avatarColor: Int,
    val headerColor: Int
)