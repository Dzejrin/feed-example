package com.example.feed_core.model

data class Story(
    val id: Long,
    val name: String,
    val coverImageUrl: String,
    val landscapeImageUrl: String,
    val shareUrl: String,
    val backgroundColor: Int,
    val likes: Int,
    val owner: User
)