package com.example.feed_core

import com.example.feed_core.networking.HttpManager

object FeedCore {
    const val STORIES_URL: String = "https://api.steller.co/v1/users/76794126980351029/stories"

    private var httpManagerClass: Class<out HttpManager>? = null

    fun setHttpManagerClass(clazz: Class<out HttpManager>) {
        this.httpManagerClass = clazz
    }

    fun newHttpManagerInstance(): HttpManager {
        if (httpManagerClass == null) {
            throw IllegalStateException("You must set HttpManager class first.")
        }

        return httpManagerClass!!.newInstance()
    }
}