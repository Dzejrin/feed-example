package com.example.feed_core.networking

import java.io.IOException
import java.io.InputStream

data class HttpResponse(
    val code: Int,
    val byteStream: InputStream?,
    val error: IOException?
)