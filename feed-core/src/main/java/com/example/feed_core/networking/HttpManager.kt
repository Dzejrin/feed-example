package com.example.feed_core.networking

interface HttpManager {
    fun request(url: String): HttpResponse
}