package com.example.feed_core.feed

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import com.example.feed_core.FeedCore
import com.example.feed_core.model.Story
import com.example.feed_core.model.User
import kotlinx.coroutines.*
import org.json.JSONObject

class FeedManager private constructor() {

    private val httpManager = FeedCore.newHttpManagerInstance()
    private val imageManager = FeedImageManager(httpManager)
    private val usersCache = HashMap<Long, User>()

    private var feedCache: List<Story>? = null

    suspend fun getFeedImages(): HashMap<Long, Bitmap>? = withContext(Dispatchers.IO) {
        val imagesByIds = HashMap<Long, Bitmap>()
        val feed = loadFeed()

        if (feed != null) {
            val iterator = feed.iterator()
            while (iterator.hasNext()) {
                val story = iterator.next()
                val image  = imageManager.loadImage(story.coverImageUrl)

                if (image != null) {
                    imagesByIds[story.id] = image

                    if (imagesByIds.count() == feed.size) {
                        return@withContext imagesByIds
                    }
                } else {
                    return@withContext null
                }
            }
        }

        return@withContext null
    }

    fun getFeedDetail(storyId: Long, callback: LoadFeedDetailCallback) {
        GlobalScope.async(Dispatchers.IO) {
            val feed = loadFeed()

            if (feed != null) {
                feed.find { it.id == storyId }?.let { story ->
                    val storyImage = imageManager.loadImage(story.coverImageUrl)
                    val avatarImage = imageManager.loadImage(story.owner.avatarImageUrl)

                    if (storyImage != null && avatarImage != null) {
                        callback.onSuccess(story, storyImage, avatarImage)
                    } else {
                        callback.onFailure()
                    }
                }
            } else {
                callback.onFailure()
            }
        }
    }

    private suspend fun loadFeed(): List<Story>? = withContext(Dispatchers.IO) {
        if (feedCache == null) {
            val response = httpManager.request(FeedCore.STORIES_URL)
            val body = response.byteStream?.readBytes()?.toString(Charsets.UTF_8)

            if (response.error == null && body != null) {
                feedCache = parseFeedResponse(body)
                feedCache
            } else {
                Log.e(TAG, response.error?.message as String)
                null
            }
        } else {
            feedCache
        }
    }

    private fun parseFeedResponse(jsonData: String): List<Story> {
        val stories: MutableList<Story> = arrayListOf()
        val jsonObject = JSONObject(jsonData)
        val storiesJsonArray = jsonObject.getJSONArray(DATA_KEY)

        for (i in 0.until(storiesJsonArray.length())) {
            val storyJsonObject = storiesJsonArray.getJSONObject(i)
            val id = storyJsonObject.getLong(ID_KEY)
            val name = storyJsonObject.getString(STORY_NAME_KEY)
            val coverImageUrl = storyJsonObject.getString(COVER_IMAGE_KEY)
            val landscapeImageUrl = storyJsonObject.getString(LANDSCAPE_IMAGE_URL_KEY)
            val shareUrl = storyJsonObject.getString(SHARE_URL_KEY)
            val backgroundColor = parseColor(storyJsonObject.getString(STORY_COLOR_KEY))
            val likesObject = storyJsonObject.getJSONObject(LIKES_OBJECT_KEY)
            val likes = likesObject.getInt(LIKES_COUNT_KEY)
            val user = getUserFromJson(storyJsonObject.getJSONObject(USER_KEY))

            val story = Story(
                id = id,
                name = name,
                coverImageUrl = coverImageUrl,
                landscapeImageUrl = landscapeImageUrl,
                shareUrl = shareUrl,
                backgroundColor = backgroundColor,
                likes = likes,
                owner = user
            )

            stories.add(story)
        }

        return stories
    }

    private fun getUserFromJson(jsonObject: JSONObject): User {
        val id = jsonObject.getLong(ID_KEY)

        if (usersCache.containsKey(id)) {
            return usersCache[id]!!
        }

        val displayName = jsonObject.getString(DISPLAY_NAME_KEY)
        val userName = jsonObject.getString(USERNAME_KEY)
        val avatarImageUrl = jsonObject.getString(AVATAR_IMAGE_URL_KEY)
        val followers = jsonObject.getInt(FOLLOWERS_KEY)
        val following = jsonObject.getInt(FOLLOWING_KEY)
        val avatarColor = parseColor(jsonObject.getString(AVATAR_COLOR_KEY))
        val headerColor = parseColor(jsonObject.getString(HEADER_COLOR_KEY))
        val user = User(id, displayName, userName, avatarImageUrl, followers, following, avatarColor, headerColor)

        usersCache[id] = user

        return user
    }

    private fun parseColor(colorCode: String): Int {
        return Color.parseColor(colorCode)
    }

    companion object {
        val instance: FeedManager = FeedManager()

        private const val TAG: String = "FeedManager"

        private const val DATA_KEY: String = "data"
        private const val ID_KEY: String = "id"

        // story
        private const val STORY_NAME_KEY: String = "title"
        private const val COVER_IMAGE_KEY: String = "cover_src"
        private const val LANDSCAPE_IMAGE_URL_KEY: String = "landscape_share_image"
        private const val SHARE_URL_KEY: String = "share_url"
        private const val STORY_COLOR_KEY: String = "cover_bg"
        private const val LIKES_OBJECT_KEY: String = "likes"
        private const val LIKES_COUNT_KEY: String = "count"
        private const val USER_KEY: String = "user"

        // user
        private const val DISPLAY_NAME_KEY: String = "display_name"
        private const val USERNAME_KEY: String = "_username"
        private const val AVATAR_IMAGE_URL_KEY: String = "avatar_image_url"
        private const val FOLLOWERS_KEY: String = "followers"
        private const val FOLLOWING_KEY: String = "following"
        private const val AVATAR_COLOR_KEY: String = "avatar_image_bg"
        private const val HEADER_COLOR_KEY: String = "header_image_bg"
    }
}