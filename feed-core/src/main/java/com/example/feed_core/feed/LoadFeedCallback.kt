package com.example.feed_core.feed

import com.example.feed_core.model.Story

interface LoadFeedCallback {
    fun onSuccess(feed: List<Story>)
    fun onFailure()
}