package com.example.feed_core.feed

import android.graphics.Bitmap

interface LoadFeedImagesCallback {
    fun onSuccess(imagesByStoryId: HashMap<Long, Bitmap>)
    fun onFailure()
}