package com.example.feed_core.feed

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.example.feed_core.networking.HttpManager

class FeedImageManager(private val httpManager: HttpManager) {

    private val imagesCache = HashMap<String, Bitmap>()

    fun loadImage(url: String): Bitmap? {
        return if (!imagesCache.containsKey(url)) {
            val response = httpManager.request(url)
            val bitmap = BitmapFactory.decodeStream(response.byteStream)

            if (response.error == null && bitmap != null) {
                imagesCache[url] = bitmap
                bitmap
            } else {
                Log.e(TAG, response.error?.message as String)
                null
            }
        } else {
            imagesCache[url]!!
        }
    }

    companion object {
        private const val TAG: String = "FeedImageManager"
    }
}