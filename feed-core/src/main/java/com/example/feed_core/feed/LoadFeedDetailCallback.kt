package com.example.feed_core.feed

import android.graphics.Bitmap
import com.example.feed_core.model.Story

interface LoadFeedDetailCallback {
    fun onSuccess(story: Story, storyImage: Bitmap, avatarImage: Bitmap)
    fun onFailure()
}