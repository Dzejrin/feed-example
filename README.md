# Feed Example

An example Android app that loads feed of stories from Steller API.

## Modules

App contains two modules:

- `app` - Phone & Tablet module
    - GUI module responsible for handling views, fragments and all the Android platform stuff
- `feed-core` - Android library
    - backend module responsible for gathering data from API

## Libraries

App uses several Android JetPack (`androidx`) libraries to ensure best compatibility as well as latest and greatest technologies. Key androidx dependencies: `ConstraintLayout`, `ViewPager2`, `Fragment`, `Navigation`

There are also few more other libraries:

- `com.squareup.retrofit2:retrofit` for HTTP comunication
- `com.mikepenz:fastadapter` for better RecyclerView adapters
- `com.github.MikeOrtiz:TouchImageView` for ImageView with built-in zoom, pinch and swipe gestures

## Screenshots

<img src="screenshots/Screenshot_20200312-210805.png" width="30%" />

<img src="screenshots/Screenshot_20200312-210819.png" width="30%" />