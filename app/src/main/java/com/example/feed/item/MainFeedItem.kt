package com.example.feed.item

import android.graphics.Bitmap
import android.view.View
import com.example.feed.R
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_feed.view.*

class MainFeedItem(val id: Long, private val image: Bitmap): AbstractItem<MainFeedItem.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.item_feed

    override val type: Int
        get() = id.toInt()

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    inner class ViewHolder(itemView: View): FastAdapter.ViewHolder<AbstractItem<*>>(itemView) {
        override fun bindView(item: AbstractItem<*>, payloads: MutableList<Any>) {
            itemView.imageView.setImageBitmap(image)
        }

        override fun unbindView(item: AbstractItem<*>) {
            itemView.imageView.setImageDrawable(null)
        }
    }
}