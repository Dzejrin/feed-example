package com.example.feed.activity

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.example.feed.R

class FeedActivity: FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
    }

}