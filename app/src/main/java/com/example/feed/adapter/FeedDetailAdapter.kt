package com.example.feed.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.feed.fragment.FeedStoryFragment

class FeedDetailAdapter(
    fragment: Fragment,
    private val storyIds: LongArray
): FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = storyIds.size

    override fun createFragment(position: Int): Fragment = FeedStoryFragment.newInstance(storyIds[position])
}