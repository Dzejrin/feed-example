package com.example.feed.extensions

import android.view.View

/**
 * Use this prevent multiple tap actions
 */
fun View.setOnSingleClickListener(action: () -> Unit) {
    var lastClickTime = 0L
    setOnClickListener {
        val now = System.currentTimeMillis()
        if (now - lastClickTime > 400) {
            action()
            lastClickTime = now
        }
    }
}