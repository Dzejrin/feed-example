package com.example.feed.utils

import com.example.feed_core.networking.HttpManager
import com.example.feed_core.networking.HttpResponse
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit

class OkHttpManager: HttpManager {

    private val client = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .callTimeout(10, TimeUnit.SECONDS)
        .build()

    override fun request(url: String): HttpResponse {
        val request = Request.Builder()
            .url(url)
            .build()

        return try {
            val response = client.newCall(request).execute()
            HttpResponse(response.code(), response.body()?.byteStream(), null)
        } catch (exception: IOException) {
            HttpResponse(0, null, exception)
        }
    }
}