package com.example.feed.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.example.feed.R
import kotlinx.android.synthetic.main.fragment_fullscreen_photo.*

class FullScreenPhotoFragment: DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.AppTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fullscreen_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Handler().postDelayed({
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        }, 500)

        val image = if (arguments?.containsKey(KEY_IMAGE) == true) {
            arguments?.getParcelable<Bitmap>(KEY_IMAGE)
        } else {
            null
        }
        ivPhoto.setImageBitmap(image)

        btnClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setWindowAnimations(R.style.DialogAnimation)
    }

    companion object {
        private const val KEY_IMAGE: String = "image"

        fun newInstance(image: Bitmap): FullScreenPhotoFragment {
            val fragment = FullScreenPhotoFragment()
            val args = Bundle()
            args.putParcelable(KEY_IMAGE, image)
            fragment.arguments = args
            return fragment
        }
    }
}