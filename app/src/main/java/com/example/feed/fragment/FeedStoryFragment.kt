package com.example.feed.fragment

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.feed.R
import com.example.feed_core.feed.FeedManager
import com.example.feed_core.feed.LoadFeedDetailCallback
import com.example.feed_core.model.Story
import com.example.feed.extensions.setOnSingleClickListener
import kotlinx.android.synthetic.main.fragment_feed_story.*

class FeedStoryFragment: Fragment(R.layout.fragment_feed_story) {

    private val feedManager = FeedManager.instance
    private var likedByMe = false
    private var likes = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { args ->
            likedByMe = args.getBoolean(KEY_LIKED_BY_ME, false)

            if (args.containsKey(KEY_STORY_ID)) {
                val storyId = args.getLong(KEY_STORY_ID)
                feedManager.getFeedDetail(storyId, object : LoadFeedDetailCallback {
                    override fun onSuccess(story: Story, storyImage: Bitmap, avatarImage: Bitmap) {
                        likes = story.likes
                        activity?.runOnUiThread {
                            showFeedDetail(story, storyImage, avatarImage)
                        }
                    }

                    override fun onFailure() {
                        if (isResumed) {
                            findNavController().navigateUp()
                        }
                    }
                })
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean(KEY_LIKED_BY_ME, likedByMe)
    }

    private fun showFeedDetail(story: Story, storyImage: Bitmap, avatarImage: Bitmap) {
        pbFeedStory.visibility = View.GONE

        ivStory.setImageBitmap(storyImage)
        ivAvatar.setImageBitmap(avatarImage)

        tvUserName.text = story.owner.displayName
        tvStoryName.text = story.name

        updateLikes()

        btnLike.setOnSingleClickListener {
            likedByMe = !likedByMe

            if (likedByMe) {
                likes++
            } else {
                likes--
            }

            updateLikes()
        }

        btnShare.setOnSingleClickListener {
            val sendIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message, story.shareUrl))
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, getString(R.string.share_story_title))
            startActivity(shareIntent)
        }

        ivStory.setOnSingleClickListener {
            val fragment = FullScreenPhotoFragment.newInstance(storyImage)
            fragment.show(childFragmentManager, TAG_FULLSCREEN_PHOTO)
        }

        userTouchArea.setOnSingleClickListener {
            findNavController().navigate(FeedDetailFragmentDirections.actionFeedDetailFragmentToUserProfileDialogFragment(
                userName = story.owner.userName,
                displayName = story.owner.displayName,
                followers = story.owner.followers,
                following = story.owner.following,
                avatarImage = avatarImage,
                headerColor = story.owner.headerColor,
                avatarColor = story.owner.avatarColor
            ))
        }
    }

    private fun updateLikes() {
        if (likedByMe) {
            btnLike.imageTintList = ColorStateList.valueOf(Color.RED)
        } else {
            btnLike.imageTintList = ColorStateList.valueOf(Color.WHITE)
        }
        tvLikesCounter.text = likes.toString()
    }

    companion object {
        private const val KEY_STORY_ID: String = "story_id"
        private const val KEY_LIKED_BY_ME: String = "liked_by_me"
        private const val TAG_FULLSCREEN_PHOTO: String = "fullscreen_photo"

        fun newInstance(storyId: Long): FeedStoryFragment {
            val fragment = FeedStoryFragment()
            val args = Bundle()
            args.putLong(KEY_STORY_ID, storyId)
            fragment.arguments = args
            return fragment
        }
    }
}