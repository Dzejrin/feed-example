package com.example.feed.fragment

import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.example.feed.R
import kotlinx.android.synthetic.main.fragment_user_profile_dialog.view.*

class UserProfileDialogFragment: DialogFragment() {

    private val args by navArgs<UserProfileDialogFragmentArgs>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(requireContext())
        val view = inflater.inflate(R.layout.fragment_user_profile_dialog, null)

        args.run {
            view.tvDisplayName.text = displayName
            view.tvUserName.text = userName
            view.tvFollowersCount.text = followers.toString()
            view.tvFollowingCount.text = following.toString()
            view.ivAvatar.setImageBitmap(avatarImage)
            view.viewHeaderBackground.background = ColorDrawable(headerColor)
            view.cardViewProfileBackground.setCardBackgroundColor(avatarColor)
        }

        return AlertDialog.Builder(requireContext())
            .setView(view)
            .create()
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setWindowAnimations(R.style.ProfileDialogAnimation)
    }
}