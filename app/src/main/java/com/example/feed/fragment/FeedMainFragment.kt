package com.example.feed.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.feed.R
import com.example.feed_core.feed.FeedManager
import com.example.feed_core.feed.LoadFeedImagesCallback
import com.example.feed.item.MainFeedItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_feed_main.*
import kotlinx.coroutines.*

class FeedMainFragment: Fragment(R.layout.fragment_feed_main), CoroutineScope by MainScope() {

    private val feedManager = FeedManager.instance
    private val itemAdapter = ItemAdapter<MainFeedItem>()
    private val fastAdapter = FastAdapter.with(itemAdapter)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.title = getString(R.string.feed_main_title)

        recyclerFeed.adapter = fastAdapter

        fastAdapter.onClickListener = { _, _, item, _ ->
            val selectedStoryId = item.id
            val storyIds = itemAdapter.adapterItems.map { it.id }.toLongArray()
            val direction = FeedMainFragmentDirections.actionFeedMainFragmentToFeedDetailFragment(selectedStoryId, storyIds)

            findNavController().navigate(direction)

            true
        }

        btnRetry.setOnClickListener {
            loadMainFeed()
        }
    }

    override fun onResume() {
        super.onResume()

        loadMainFeed()
    }

    private fun loadMainFeed() {
        showLoading()

        launch {
            val imagesByStoryId = feedManager.getFeedImages()

            if (isResumed) {
                imagesByStoryId?.let {
                    showMainFeed(imagesByStoryId)
                } ?: run {
                    showError()
                }
            }
        }
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
        tvFeedLoading.visibility = View.VISIBLE
        tvErrorTitle.visibility = View.INVISIBLE
        tvErrorMessage.visibility = View.INVISIBLE
        btnRetry.visibility = View.INVISIBLE
        recyclerFeed.visibility = View.INVISIBLE
    }

    private fun showMainFeed(imagesByIds: HashMap<Long, Bitmap>) {
        progressBar.visibility = View.INVISIBLE
        tvFeedLoading.visibility = View.INVISIBLE
        tvErrorTitle.visibility = View.INVISIBLE
        tvErrorMessage.visibility = View.INVISIBLE
        btnRetry.visibility = View.INVISIBLE
        recyclerFeed.visibility = View.VISIBLE

        itemAdapter.clear()

        val ids = imagesByIds.keys.toLongArray()
        ids.sortDescending()

        for (id in ids) {
            itemAdapter.add(MainFeedItem(id, imagesByIds[id]!!))
        }
    }

    private fun showError() {
        progressBar.visibility = View.INVISIBLE
        tvFeedLoading.visibility = View.INVISIBLE
        tvErrorTitle.visibility = View.VISIBLE
        tvErrorMessage.visibility = View.VISIBLE
        btnRetry.visibility = View.VISIBLE
        recyclerFeed.visibility = View.INVISIBLE
    }
}