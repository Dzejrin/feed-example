package com.example.feed.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.example.feed.R
import com.example.feed.adapter.FeedDetailAdapter
import kotlinx.android.synthetic.main.fragment_feed_detail.*


class FeedDetailFragment: Fragment(R.layout.fragment_feed_detail) {

    private val args by navArgs<FeedDetailFragmentArgs>()

    private lateinit var adapter: FeedDetailAdapter
    private lateinit var storyIds: LongArray

    @SuppressLint("PrivateResource")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.title = getString(R.string.feed_detail_title)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        storyIds = args.storyIds
        adapter = FeedDetailAdapter(this, storyIds)

        pagerFeed.adapter = adapter
        pagerFeed.setCurrentItem(storyIds.indexOf(args.selectedStoryId), false)

        pagerFeed.setPageTransformer { page, position ->
            page.cameraDistance = 20000f
            page.pivotX = if (position <= 0) view.width.toFloat() else 0.0f
            page.pivotY = view.height * 0.5f
            page.rotationY = 90f * position
        }
        
        pagerFeed.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                updateButtonsVisibility()
            }
        })

        btnNext.setOnClickListener {
            pagerFeed.currentItem++
        }

        btnPrevious.setOnClickListener {
            pagerFeed.currentItem--
        }

        updateButtonsVisibility()
    }

    private fun updateButtonsVisibility() {

        btnPrevious.visibility = if (pagerFeed.currentItem == 0) {
            View.GONE
        } else {
            View.VISIBLE
        }

        btnNext.visibility = if (pagerFeed.currentItem == storyIds.size - 1) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}