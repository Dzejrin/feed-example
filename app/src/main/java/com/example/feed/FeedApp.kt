package com.example.feed

import android.app.Application
import com.example.feed.utils.OkHttpManager
import com.example.feed_core.FeedCore

class FeedApp: Application() {

    override fun onCreate() {
        super.onCreate()

        init()
    }

    private fun init() {
        FeedCore.setHttpManagerClass(OkHttpManager::class.java)
    }
}